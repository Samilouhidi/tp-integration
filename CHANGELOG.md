# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.0.0] - 2017-06-20
### Added
- The NavBar
- The background
- The presentation Text

### Removed 
- all the rest of the page :) 